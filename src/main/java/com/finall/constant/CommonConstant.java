package com.finall.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommonConstant {

    public static final String EMPTY_STRING = "";

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ApiErrorStringConstant {
        public static final String DUPLICATED_USERNAME = "The username is existed!";
        public static final String ENTITY_NOT_FOUND = "The Entity is NOT found!";
        public static final String USERNAME_OR_PASSWORD_INCORRECT = "Username or Password is incorrect!";
        public static final String FILE_UPLOAD_FAILED = "Failed when uploading file!";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class FieldNameConstant {
        public static final String USERNAME = "username";
        public static final String EMPLOYEE_ID = "employeeID";
        public static final String FILE_ID = "fileID";
        public static final String URL = "url";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class EntityNameConstant {
        public static final String EMPLOYEE = "Employee";
        public static final String FILE = "File";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class FileConstant {
        public static final String ZIP_EXTENSION = ".zip";
        public static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";
        public static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
        public static final String KEY_SLASH = "\"";
        public static final String COMPRESSES_FILE = "Compressed-files";
        public static final String KILOBYTE = "KB";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AttributeConstant {
        public static final String EMPLOYEE_LOGGED_IN = "employeeLoggedIn";
        public static final String EMPLOYEE_ID = "employeeID";
        public static final String PERMISSION_FILES = "permissionFiles";
        public static final String FORM_DOWNLOAD = "formDownloadRequestDTO";
        public static final String FILE_FILTER_FORM = "fileFilterRequestDTO";
        public static final String CURRENT_FILE_VIEW = "currentFileView";
        public static final String CURRENT_FILE_ID = "currentFileID";
        public static final String EMPLOYEE_LIST = "employeeList";

        public static final String SUCCESS_REGISTER_EMPLOYEE = "success_RegisterEmployee";
        public static final String SUCCESS_GRANT_PERMISSION = "success_GrantPermission";

        public static final String ERROR_UPLOAD_FILE = "error_UploadFile";
        public static final String ERROR_NO_PERMISSION = "error_NoPermission";

        public static final List<String> ALL_ATTRIBUTES = Arrays.asList(
                EMPLOYEE_LOGGED_IN,
                EMPLOYEE_ID,
                CURRENT_FILE_VIEW,
                CURRENT_FILE_ID,
                PERMISSION_FILES
        );
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class TemplateConstant {
        public static final String REDIRECT = "redirect:/";
        public static final String INDEX = "index";
        public static final String HOME = "home";
        public static final String LOGIN = "employee/login";
        public static final String REGISTER = "employee/register";
        public static final String FILE_HOME = "file";
        public static final String FILE_PERMISSION_EDIT = "file-permission-edit";
    }
}
