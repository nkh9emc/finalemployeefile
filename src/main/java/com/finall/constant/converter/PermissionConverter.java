package com.finall.constant.converter;

import com.finall.constant.PermissionEnum;

import javax.persistence.AttributeConverter;

public class PermissionConverter implements AttributeConverter<PermissionEnum, String> {
    @Override
    public String convertToDatabaseColumn(PermissionEnum attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getValue();
    }

    @Override
    public PermissionEnum convertToEntityAttribute(String dbData) {
        return PermissionEnum.findByString(dbData);
    }
}
