package com.finall.constant;

import com.fasterxml.jackson.annotation.JsonValue;

public enum PermissionEnum {
    OWNER("O"),
    SHARE("S"),
    READ("R"),
    DISABLED("D");

    private String value;

    PermissionEnum(String value) {
        this.value = value;
    }

    public static PermissionEnum findByString(String s) {
        for (PermissionEnum employeeType : PermissionEnum.values()) {
            if (employeeType.getValue().equalsIgnoreCase(s)) return employeeType;
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return this.value;
    }

}
