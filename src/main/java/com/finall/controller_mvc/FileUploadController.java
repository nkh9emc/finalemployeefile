package com.finall.controller_mvc;

import com.finall.service.FileService;
import com.finall.service.PermissionService;
import com.finall.utils.PermissionUtil;
import com.finall.utils.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

import static com.finall.constant.CommonConstant.AttributeConstant.EMPLOYEE_ID;
import static com.finall.constant.CommonConstant.AttributeConstant.ERROR_UPLOAD_FILE;
import static com.finall.constant.CommonConstant.TemplateConstant.HOME;
import static com.finall.constant.CommonConstant.TemplateConstant.REDIRECT;

@Controller
@Slf4j
@RequestMapping(path = "/employee/{employeeID}/file")
@AllArgsConstructor
public class FileUploadController {

    private final FileService fileService;
    private final PermissionService permissionService;
    private final PermissionUtil permissionUtil;

    @PostMapping
    public String uploadFile(Model model,
                             @PathVariable(value = "employeeID") Long employeeID,
                             @RequestPart MultipartFile[] files) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            Long employeeIDFromSession = (Long) session.getAttribute(EMPLOYEE_ID);
            if (employeeIDFromSession != null && employeeIDFromSession.equals(employeeID)) {
                fileService.uploadFile(employeeID, files);
                model.addAttribute(ERROR_UPLOAD_FILE, null);
            } else
                model.addAttribute(ERROR_UPLOAD_FILE, true);
        }
        return REDIRECT + HOME;

    }
}
