package com.finall.controller_mvc;

import com.finall.model.request.DownloadFromFormRequestDTO;
import com.finall.model.request.PermissionFileFilterRequestDTO;
import com.finall.model.response.PermissionResponseDTO;
import com.finall.service.PermissionService;
import com.finall.utils.PermissionUtil;
import com.finall.utils.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.finall.constant.CommonConstant.AttributeConstant.*;
import static com.finall.constant.CommonConstant.TemplateConstant.*;

@Controller
@Slf4j
@RequestMapping(path = "/")
@AllArgsConstructor
public class HomeController {

    private final PermissionUtil permissionUtil;
    private final PermissionService permissionService;

    @GetMapping("/")
    public String index() {
        if (permissionUtil.isLoggedIn()) {
            return REDIRECT + HOME;
        } else
            return INDEX;
    }

    @GetMapping("/home")
    public String home(Model model) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            Long employeeIDFromSession = (Long) session.getAttribute(EMPLOYEE_ID);
            //  Get all Files
            List<PermissionResponseDTO> permissionFiles;
            if (session.getAttribute(PERMISSION_FILES) != null)
                permissionFiles = (List<PermissionResponseDTO>) session.getAttribute(PERMISSION_FILES);
            else
                permissionFiles = permissionService.findPermissionOfAnEmployee(employeeIDFromSession);

            model.addAttribute(PERMISSION_FILES, permissionFiles);
            session.removeAttribute(PERMISSION_FILES);

            //  Form download
            model.addAttribute(FORM_DOWNLOAD, new DownloadFromFormRequestDTO());
            model.addAttribute(FILE_FILTER_FORM, new PermissionFileFilterRequestDTO());
            return HOME;
        } else
            return INDEX;
    }
}
