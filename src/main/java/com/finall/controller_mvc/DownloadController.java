package com.finall.controller_mvc;

import com.finall.service.FileService;
import com.finall.utils.PermissionUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.finall.constant.CommonConstant.FileConstant.*;

@Controller
@Slf4j
@RequestMapping(path = "/download")
@AllArgsConstructor
public class DownloadController {

    private final FileService fileService;
    private final PermissionUtil permissionUtil;

    @GetMapping
    public ResponseEntity<Resource> downloadFile(
            @RequestParam(value = "fileID") Long fileID,
            HttpServletRequest request) throws IOException {
        if (permissionUtil.isLoggedIn()) {
            Resource resource = fileService.downloadFileByFileID(fileID);
            String contentType = null;
            try {
                contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (Exception e) {
                log.error("Could not get the content type!");
            }
            contentType = contentType != null ? contentType : DEFAULT_CONTENT_TYPE;

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, ATTACHMENT_FILENAME + resource.getFilename() + KEY_SLASH)
                    .body(resource);
        }
        return null;
    }

    @GetMapping(path = "/zip")
    public ResponseEntity<Resource> downloadAsZip(
            @RequestParam(value = "fileID") Long fileID,
            HttpServletRequest request) throws IOException {
        if (permissionUtil.isLoggedIn()) {
            Set<Long> ids = new HashSet<>();
            ids.add(fileID);
            Resource resource = fileService.zipAndDownloadFiles(ids);
            String contentType = null;
            try {
                contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (Exception e) {
                log.error("Could not get the content type!");
            }
            contentType = contentType != null ? contentType : DEFAULT_CONTENT_TYPE;

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, ATTACHMENT_FILENAME + resource.getFilename() + KEY_SLASH)
                    .body(resource);
        }
        return null;
    }
}
