package com.finall.controller_mvc.filter;

public interface SharePermissionRequired extends LoginRequired {

    boolean hasSharePermission(Long employeeID, Long fileID);
}
