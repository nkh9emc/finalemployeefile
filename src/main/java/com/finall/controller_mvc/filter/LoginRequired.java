package com.finall.controller_mvc.filter;

public interface LoginRequired {

    boolean isLoggedIn();
}
