package com.finall.controller_mvc.filter;

public interface OwnerPermissionRequired extends LoginRequired {

    boolean hasOwnerPermission(Long ownerID, Long fileID);
}
