package com.finall.controller_mvc.filter;

public interface ReadPermissionRequired extends LoginRequired {

    boolean hasReadPermission(Long employeeID, Long fileID);
}
