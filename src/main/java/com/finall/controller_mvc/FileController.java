package com.finall.controller_mvc;

import com.finall.model.request.PermissionFileFilterRequestDTO;
import com.finall.model.request.PermissionRequestDTO;
import com.finall.model.response.EmployeeResponseDTO;
import com.finall.model.response.FileResponseDTO;
import com.finall.model.response.PermissionResponseDTO;
import com.finall.service.EmployeeService;
import com.finall.service.FileService;
import com.finall.service.PermissionService;
import com.finall.utils.EntityUtil;
import com.finall.utils.PermissionUtil;
import com.finall.utils.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

import static com.finall.constant.CommonConstant.AttributeConstant.*;
import static com.finall.constant.CommonConstant.TemplateConstant.*;

@Controller
@Slf4j
@RequestMapping(path = "/file")
@AllArgsConstructor
public class FileController {
    private final FileService fileService;
    private final PermissionService permissionService;
    private final EmployeeService employeeService;

    private final PermissionUtil permissionUtil;
    private final EntityUtil entityUtil;

    @GetMapping(path = "/{fileID}")
    public String fileHome(Model model,
                           @PathVariable("fileID") Long fileID) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            String fileName = fileService.getFileNameByID(fileID);
            session.setAttribute(CURRENT_FILE_VIEW, fileName);
            session.setAttribute(CURRENT_FILE_ID, fileID);

            List<PermissionResponseDTO> filePermissions = permissionService.findPermissionsOfAFile(fileID);
            model.addAttribute(PERMISSION_FILES, filePermissions);
            return FILE_HOME;
        } else
            return LOGIN;
    }

    @GetMapping(path = "/{fileID}/delete")
    public String deleteFile(Model model,
                             @PathVariable("fileID") Long fileID) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            Long ownerID = (Long) session.getAttribute(EMPLOYEE_ID);
            if (permissionUtil.hasOwnerPermission(ownerID, fileID)) {
                //  Success
                List<FileResponseDTO> deletingFiles = fileService.deleteFileInDB(ownerID, Collections.singleton(fileID));
                fileService.deleteFileInDirectory(deletingFiles);
            } else {
                model.addAttribute(ERROR_NO_PERMISSION, true);
            }
            return REDIRECT + HOME;
        } else
            return LOGIN;
    }

    @GetMapping(path = "/{fileID}/edit-permission")
    public String changePermissionHome(Model model,
                                       @PathVariable("fileID") Long fileID) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            Long ownerID = (Long) session.getAttribute(EMPLOYEE_ID);
            if (permissionUtil.hasOwnerPermission(ownerID, fileID)
                    || permissionUtil.hasSharePermission(ownerID, fileID)) {
                //  Success
                session.setAttribute(CURRENT_FILE_ID, fileID);

                List<EmployeeResponseDTO> employeeList = employeeService.findAllExceptCurrentOwner(ownerID);
                model.addAttribute(EMPLOYEE_LIST, employeeList);

                model.addAttribute("permissionRequestDTO", new PermissionRequestDTO());
                return FILE_PERMISSION_EDIT;
            } else {
                model.addAttribute(ERROR_NO_PERMISSION, true);
                return FILE_HOME;
            }
        } else
            return LOGIN;
    }

    @PostMapping(path = "/{fileID}/edit-permission")
    public String grantPermission(Model model,
                                  @PathVariable("fileID") Long fileID,
                                  @ModelAttribute("permissionRequestDTO") PermissionRequestDTO permissionRequestDTO) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            Long ownerID = (Long) session.getAttribute(EMPLOYEE_ID);

            permissionRequestDTO.setOwnerID(ownerID);
            permissionRequestDTO.setFileID(fileID);

            permissionService.grantPermission(permissionRequestDTO);
            model.addAttribute(SUCCESS_GRANT_PERMISSION, true);
            return REDIRECT + FILE_HOME + "/" + fileID;
        } else
            return LOGIN;
    }

    @PostMapping(path = "/filter")
    public String grantPermission(Model model,
                                  @ModelAttribute("fileFilterRequestDTO") PermissionFileFilterRequestDTO requestDTO) {
        if (permissionUtil.isLoggedIn()) {
            HttpSession session = Validator.getSession();
            Long employeeID = (Long) session.getAttribute(EMPLOYEE_ID);

            requestDTO.setEmployeeID(employeeID);
            List<PermissionResponseDTO> filterFiles = permissionService.findPermissionByFilter(requestDTO);
            session.setAttribute(PERMISSION_FILES, filterFiles);
            return REDIRECT + HOME;
        } else
            return LOGIN;
    }
}
