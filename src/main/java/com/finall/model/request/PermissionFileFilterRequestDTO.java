package com.finall.model.request;

import com.finall.constant.PermissionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PermissionFileFilterRequestDTO {

    private Long employeeID;

    private String searchValue;

    private PermissionEnum permissionEnum;
}
