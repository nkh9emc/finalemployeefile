package com.finall.model.request;

import com.finall.constant.PermissionEnum;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PermissionRequestDTO {

    private Long fileID;

    private Long ownerID; //    Owner of the file

    private Long grantID; // Employee granting permission

    private PermissionEnum permissionEnum;
}
