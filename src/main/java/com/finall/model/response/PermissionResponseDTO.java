package com.finall.model.response;

import com.finall.constant.PermissionEnum;
import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PermissionResponseDTO extends BaseResponseDTO {

    private Long permissionID;

    private Long fileID;
    private String fileName;

    private Long employeeID;
    private String employeeName;

    private PermissionEnum permission;
    private LocalDateTime createdOn;
}
