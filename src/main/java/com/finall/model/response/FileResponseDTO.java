package com.finall.model.response;

import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileResponseDTO extends BaseResponseDTO {

    private Long fileID;
    private Long employeeID;

    private String fileName;
    private String url;

    private LocalDateTime createdOn;

    private String fileSizeInMegaByte;
}
