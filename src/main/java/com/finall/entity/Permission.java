package com.finall.entity;

import com.finall.constant.PermissionEnum;
import com.finall.constant.converter.PermissionConverter;
import lombok.*;

import javax.persistence.*;

@Table(name = "FINAL_PERMISSION")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Permission extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERMISSION_ID", unique = true, nullable = false)
    private Long permissionID;

    @ManyToOne
    @JoinColumn(name = "FILE_ID", unique = true, nullable = false)
    private EmployeeFile employeeFile;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID", unique = true, nullable = false)
    private Employee employee;

    @Column(name = "PERMISSION")
    @Convert(converter = PermissionConverter.class)
    private PermissionEnum permissionEnum;
}
