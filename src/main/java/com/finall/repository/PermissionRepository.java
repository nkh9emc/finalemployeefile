package com.finall.repository;

import com.finall.constant.PermissionEnum;
import com.finall.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN TRUE ELSE FALSE END " +
            "FROM Permission p WHERE p.deleted = FALSE " +
            "AND p.employee.employeeID = :employeeID " +
            "AND p.employeeFile.fileID = :fileID " +
            "AND p.permissionEnum IN :permissionEnums")
    boolean hasPermissionInSet(Long employeeID, Long fileID, Set<PermissionEnum> permissionEnums);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN TRUE ELSE FALSE END " +
            "FROM Permission p WHERE p.deleted = FALSE " +
            "AND p.employee.employeeID = :employeeID " +
            "AND p.employeeFile.fileID = :fileID " +
            "AND p.permissionEnum = :permissionEnum")
    boolean hasPermission(Long employeeID, Long fileID, PermissionEnum permissionEnum);

    @Query("SELECT p " +
            "FROM Permission p " +
            "WHERE p.permissionEnum <> com.finall.constant.PermissionEnum.DISABLED " +
            "AND p.deleted = FALSE " +
            "AND p.employeeFile.fileID = :fileID")
    List<Permission> findAllPermissionByFileID(Long fileID);

    @Query("SELECT p " +
            "FROM Permission p " +
            "WHERE p.permissionEnum <> com.finall.constant.PermissionEnum.DISABLED " +
            "AND p.deleted = FALSE " +
            "AND p.employee.employeeID = :employeeID " +
            "AND p.employeeFile.deleted = FALSE ")
    List<Permission> findAllByEmployeeID(Long employeeID);

    Permission getOneByEmployee_EmployeeIDAndEmployeeFile_FileID(Long grantID, Long fileID);

    @Query("SELECT p FROM Permission p " +
            "WHERE p.permissionEnum <> com.finall.constant.PermissionEnum.DISABLED " +
            "AND p.deleted = FALSE " +
            "AND p.employeeFile.deleted = FALSE " +
            "AND p.employee.employeeID = :employeeID " +
            "AND (:searchValue IS NULL OR p.employeeFile.fileName LIKE TRIM(LOWER(CONCAT('%', CONCAT(:searchValue, '%'))))) " +
            "AND (:filterPermission IS NULL OR p.permissionEnum = :filterPermission)")
    List<Permission> findByFilter(String searchValue, PermissionEnum filterPermission, Long employeeID);
}
