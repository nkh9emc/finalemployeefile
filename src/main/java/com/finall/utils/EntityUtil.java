package com.finall.utils;

import com.finall.entity.Employee;
import com.finall.entity.EmployeeFile;
import com.finall.exception.CustomException;
import com.finall.repository.EmployeeRepository;
import com.finall.repository.FileRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.finall.constant.CommonConstant.EntityNameConstant.EMPLOYEE;
import static com.finall.constant.CommonConstant.EntityNameConstant.FILE;
import static com.finall.constant.CommonConstant.FieldNameConstant.*;

@Component
@AllArgsConstructor(onConstructor_ = {@Autowired})
@NoArgsConstructor
@Slf4j
public class EntityUtil {

    private EmployeeRepository employeeRepository;
    private FileRepository fileRepository;

    public Employee getEmployeeEntity(Long employeeID) {
        Employee employee = employeeRepository.getByEmployeeIDAndDeletedIsFalse(employeeID);
        if (employee == null)
            throw new CustomException(ExceptionGenerator.notFound(EMPLOYEE, EMPLOYEE_ID, employeeID));

        return employee;
    }


    public Employee getEmployeeEntity(String username) {
        Employee employee = employeeRepository.getByUsernameAndDeletedIsFalse(username);
        if (employee == null)
            throw new CustomException(ExceptionGenerator.notFound(EMPLOYEE, USERNAME, username));

        return employee;
    }

    public EmployeeFile getFileEntity(Long fileID) {
        EmployeeFile file = fileRepository.getByFileIDAndDeletedIsFalse(fileID);
        if (file == null)
            throw new CustomException(ExceptionGenerator.notFound(FILE, FILE_ID, fileID));

        return file;
    }
}
