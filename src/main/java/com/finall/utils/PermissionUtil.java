package com.finall.utils;

import com.finall.constant.CommonConstant;
import com.finall.constant.PermissionEnum;
import com.finall.controller_mvc.filter.LoginRequired;
import com.finall.controller_mvc.filter.OwnerPermissionRequired;
import com.finall.controller_mvc.filter.ReadPermissionRequired;
import com.finall.controller_mvc.filter.SharePermissionRequired;
import com.finall.entity.Employee;
import com.finall.entity.EmployeeFile;
import com.finall.repository.PermissionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@AllArgsConstructor(onConstructor_ = {@Autowired})
@Slf4j
public class PermissionUtil implements LoginRequired, OwnerPermissionRequired, ReadPermissionRequired, SharePermissionRequired {

    private PermissionRepository permissionRepository;
    private EntityUtil entityUtil;

    @Override
    public boolean hasOwnerPermission(Long ownerID, Long fileID) {
        Employee employee = entityUtil.getEmployeeEntity(ownerID);
        EmployeeFile file = entityUtil.getFileEntity(fileID);
        return permissionRepository.hasPermission(employee.getEmployeeID(), file.getFileID(), PermissionEnum.OWNER);
    }

    @Override
    public boolean hasReadPermission(Long employeeID, Long fileID) {
        Employee employee = entityUtil.getEmployeeEntity(employeeID);
        EmployeeFile file = entityUtil.getFileEntity(fileID);
        return permissionRepository.hasPermissionInSet(employee.getEmployeeID(), file.getFileID(),
                Stream.of(PermissionEnum.OWNER, PermissionEnum.SHARE, PermissionEnum.READ).collect(Collectors.toSet()));
    }

    @Override
    public boolean hasSharePermission(Long employeeID, Long fileID) {
        Employee employee = entityUtil.getEmployeeEntity(employeeID);
        EmployeeFile file = entityUtil.getFileEntity(fileID);
        return permissionRepository.hasPermissionInSet(employee.getEmployeeID(), file.getFileID(),
                Stream.of(PermissionEnum.OWNER, PermissionEnum.SHARE).collect(Collectors.toSet()));
    }

    @Override
    public boolean isLoggedIn() {
        HttpSession session = Validator.getSession();
        return session.getAttribute(CommonConstant.AttributeConstant.EMPLOYEE_LOGGED_IN) != null;
    }
}
