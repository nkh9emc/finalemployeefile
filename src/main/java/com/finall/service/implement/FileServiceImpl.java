package com.finall.service.implement;

import com.finall.constant.PermissionEnum;
import com.finall.entity.Employee;
import com.finall.entity.EmployeeFile;
import com.finall.entity.Permission;
import com.finall.exception.CustomException;
import com.finall.model.response.FileResponseDTO;
import com.finall.repository.EmployeeRepository;
import com.finall.repository.FileRepository;
import com.finall.repository.PermissionRepository;
import com.finall.service.FileService;
import com.finall.utils.EntityUtil;
import com.finall.utils.ExceptionGenerator;
import com.finall.utils.FileUtil;
import com.finall.utils.PermissionUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static com.finall.constant.CommonConstant.EntityNameConstant.FILE;
import static com.finall.constant.CommonConstant.FieldNameConstant.FILE_ID;
import static com.finall.constant.CommonConstant.FileConstant.COMPRESSES_FILE;
import static com.finall.constant.CommonConstant.FileConstant.KILOBYTE;

@AllArgsConstructor
@Service
@Slf4j
public class FileServiceImpl implements FileService {

    private final EmployeeRepository employeeRepository;
    private final FileRepository fileRepository;
    private final PermissionRepository permissionRepository;

    private final EntityUtil entityUtil;
    private final PermissionUtil permissionUtil;

    @Qualifier("uploadPath")
    private final Path uploadPath;

    @Qualifier("tempPath")
    private final Path tempPath;

    @Transactional
    @Override
    public List<FileResponseDTO> uploadFile(Long employeeID, MultipartFile[] files) {
        Employee employee = entityUtil.getEmployeeEntity(employeeID);

        List<EmployeeFile> employeeFileList = new ArrayList<>();
        for (MultipartFile file : files) {
            try {
                Set<Permission> permissionSet = new HashSet<>();

                Path uploadURL = FileUtil.storeFile(uploadPath, employeeID, file);
                EmployeeFile employeeFile = EmployeeFile.builder()
                        .employee(employee)
                        .fileName(file.getOriginalFilename())
                        .url(uploadPath.toAbsolutePath().relativize(uploadURL.toAbsolutePath()).toString())
                        .build();

                Permission ownerPermission = Permission.builder()
                        .employee(employee)
                        .employeeFile(employeeFile)
                        .permissionEnum(PermissionEnum.OWNER)
                        .build();

                permissionSet.add(ownerPermission);
                employeeFile.setPermissionSet(permissionSet);
                employeeFileList.add(employeeFile);
            } catch (Exception e) {
                log.error("Error uploading {} to employee {}", file.getOriginalFilename(), employee.getUsername());
            }
        }

        employeeFileList = fileRepository.saveAll(employeeFileList);
        return employeeFileList.stream().map(this::toFileResponseDTO).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<FileResponseDTO> deleteFileInDB(Long employeeID, Set<Long> fileIDs) {
        entityUtil.getEmployeeEntity(employeeID);
        Set<Long> authorizedFileIds = new HashSet<>();

        for (Long fileID : fileIDs) {
            if (permissionUtil.hasOwnerPermission(employeeID, fileID))
                authorizedFileIds.add(fileID);
        }

        List<EmployeeFile> files = fileRepository.findAllByFileIDInAndDeletedIsFalse(authorizedFileIds);
        files.forEach(file -> file.setDeleted(true));
        files = fileRepository.saveAll(files);
        return files.stream().map(this::toFileResponseDTO).collect(Collectors.toList());
    }

    @Override
    public List<FileResponseDTO> deleteFileInDirectory(List<FileResponseDTO> fileResponseDTOList) {
        Iterator fileIterator = fileResponseDTOList.iterator();

        while ((fileIterator.hasNext())) {
            FileResponseDTO fileResponseDTO = (FileResponseDTO) fileIterator.next();
            boolean deleteSuccess = FileUtil.deleteFileByUrl(uploadPath.toAbsolutePath().resolve(fileResponseDTO.getUrl()).toString());
            if (deleteSuccess) {
                log.info("File {} of employee {} has been deleted SUCCESSFULLY!", fileResponseDTO.getFileName(), fileResponseDTO.getEmployeeID());
            } else {
                log.error("File {} of employee {} was FAILED to delete!", fileResponseDTO.getFileName(), fileResponseDTO.getEmployeeID());
                fileIterator.remove();
            }
        }

        return fileResponseDTOList;
    }

    @Override
    public Resource downloadFileByFileID(Long fileID) throws IOException {
        EmployeeFile file = fileRepository.getByFileIDAndDeletedIsFalse(fileID);
        if (file == null)
            throw new CustomException(ExceptionGenerator.notFound(FILE, FILE_ID, fileID));

        return FileUtil.download(uploadPath.toAbsolutePath().resolve(file.getUrl()));
    }

    @Override
    public Resource zipAndDownloadFiles(Set<Long> fileIds) throws IOException {
        List<EmployeeFile> downloadFileList = fileRepository.findAllByFileIDInAndDeletedIsFalse(fileIds);

        if (CollectionUtils.isEmpty(downloadFileList))
            return null;

        List<File> filesToZip = new ArrayList<>();
        downloadFileList.forEach(employeeFile -> {
            File file = new File(uploadPath.toAbsolutePath().resolve(employeeFile.getUrl()).toString());
            if (file.exists()) {
                filesToZip.add(file);
            }
        });

        String zipFileUrl = FileUtil.zipFiles(tempPath, COMPRESSES_FILE, filesToZip);
        return FileUtil.download(Paths.get(zipFileUrl));
    }

    @Transactional
    @Override
    public List<FileResponseDTO> deleteAllFileOfEmployee(Long employeeID) {
        entityUtil.getEmployeeEntity(employeeID);

        List<EmployeeFile> employeeFileList = fileRepository.findAllByEmployee_EmployeeIDAndDeletedIsFalse(employeeID);

        employeeFileList.forEach(employeeFile -> employeeFile.setDeleted(true));
        employeeFileList = fileRepository.saveAll(employeeFileList);

        List<FileResponseDTO> fileResponseDTOList = employeeFileList.stream().map(this::toFileResponseDTO).collect(Collectors.toList());
        return deleteFileInDirectory(fileResponseDTOList);
    }

    /*  ADD-ONs */
    private FileResponseDTO toFileResponseDTO(EmployeeFile file) {
        Path filePath = uploadPath.resolve(file.getUrl()).toAbsolutePath();
        File currentFile = new File(filePath.toString());

        return FileResponseDTO.builder()
                .employeeID(file.getEmployee().getEmployeeID())
                .fileID(file.getFileID())
                .fileName(file.getFileName())
                .url(file.getUrl())
                .createdOn(file.getCreatedOn())
                .fileSizeInMegaByte((currentFile.length() / 1024) + KILOBYTE)
                .build();
    }

    //  Share file permission check
    @Override
    public String getFileNameByID(Long fileID) {
        EmployeeFile file = entityUtil.getFileEntity(fileID);

        return file.getFileName();
    }
}
