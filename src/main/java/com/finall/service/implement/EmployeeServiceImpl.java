package com.finall.service.implement;

import com.finall.entity.Employee;
import com.finall.exception.CustomException;
import com.finall.model.request.EmployeeCreateRequestDTO;
import com.finall.model.response.EmployeeResponseDTO;
import com.finall.repository.EmployeeRepository;
import com.finall.service.EmployeeService;
import com.finall.service.FileService;
import com.finall.utils.EntityUtil;
import com.finall.utils.ExceptionGenerator;
import com.finall.utils.Validator;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

import static com.finall.constant.CommonConstant.AttributeConstant.ALL_ATTRIBUTES;

@Service
@Slf4j
@AllArgsConstructor(onConstructor_ = {@Autowired})
@NoArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;
    private FileService fileService;
    private EntityUtil entityUtil;

    @Transactional
    @Override
    public EmployeeResponseDTO createEmployee(EmployeeCreateRequestDTO createRequestDTO) {
        if (employeeRepository.isDuplicateUsername(createRequestDTO.getUsername()))
            throw new CustomException(ExceptionGenerator.duplicateUsername(createRequestDTO.getUsername()));

        Employee employee = Employee.builder()
                .username(createRequestDTO.getUsername())
                .password(Validator.generateHashPassword(createRequestDTO.getUsername(), createRequestDTO.getPassword()))
                .build();

        employeeRepository.save(employee);

        return toEmployeeResponseDTO(employee);
    }

    @Transactional
    @Override
    public String deleteEmployee(Long employeeID) {
        Employee employee = entityUtil.getEmployeeEntity(employeeID);
        fileService.deleteAllFileOfEmployee(employeeID);
        employee.setDeleted(true);
        employeeRepository.save(employee);
        return String.format("User %s was successfully deleted.", employee.getUsername());

    }

    @Override
    public EmployeeResponseDTO validateLogin(EmployeeCreateRequestDTO requestDTO) {
        Employee employee = employeeRepository.getByUsernameAndDeletedIsFalse(requestDTO.getUsername());
        if (employee == null)
            throw new CustomException(ExceptionGenerator.invalidLogin());

        if (!employee.getPassword().equals(Validator.generateHashPassword(requestDTO.getUsername(), requestDTO.getPassword())))
            throw new CustomException(ExceptionGenerator.invalidLogin());

        return toEmployeeResponseDTO(employee);
    }

    @Override
    public void clearSession() {
        HttpSession session = Validator.getSession();
        for (String att : ALL_ATTRIBUTES) {
            session.removeAttribute(att);
        }
    }

    @Override
    public List<EmployeeResponseDTO> findAllExceptCurrentOwner(Long currentOwnerID) {
        List<Employee> employeeList = employeeRepository.findAllExceptCurrentOwner(currentOwnerID);

        return employeeList.stream().map(this::toEmployeeResponseDTO).collect(Collectors.toList());
    }

    private EmployeeResponseDTO toEmployeeResponseDTO(Employee employee) {
        EmployeeResponseDTO responseDTO = EmployeeResponseDTO.builder()
                .employeeID(employee.getEmployeeID())
                .username(employee.getUsername())
                .build();

        responseDTO.setCreatedOn(employee.getCreatedOn());
        responseDTO.setDeleted(employee.isDeleted());
        responseDTO.setLastModified(employee.getLastModified());
        return responseDTO;
    }
}
