package com.finall.service.implement;

import com.finall.constant.PermissionEnum;
import com.finall.entity.Employee;
import com.finall.entity.EmployeeFile;
import com.finall.entity.Permission;
import com.finall.exception.CustomException;
import com.finall.model.request.PermissionFileFilterRequestDTO;
import com.finall.model.request.PermissionRequestDTO;
import com.finall.model.response.PermissionResponseDTO;
import com.finall.repository.PermissionRepository;
import com.finall.service.PermissionService;
import com.finall.utils.EntityUtil;
import com.finall.utils.ExceptionGenerator;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class PermissionServiceImpl implements PermissionService {

    private PermissionRepository permissionRepository;
    private EntityUtil entityUtil;

    @Transactional
    @Override
    public PermissionResponseDTO grantPermission(PermissionRequestDTO dto) {
        Employee owner = entityUtil.getEmployeeEntity(dto.getOwnerID());
        EmployeeFile file = entityUtil.getFileEntity(dto.getFileID());

        if (!hasGrantPermission(dto.getOwnerID(), dto.getFileID(), dto.getPermissionEnum()))
            throw new CustomException(ExceptionGenerator.noGrantPermission(owner.getUsername(), file.getFileName(), dto.getPermissionEnum().getValue()));

        Employee granted = entityUtil.getEmployeeEntity(dto.getGrantID());

        Permission permission = permissionRepository.getOneByEmployee_EmployeeIDAndEmployeeFile_FileID(dto.getGrantID(), dto.getFileID());
        permission = toPermissionEntity(permission, granted, file, dto.getPermissionEnum());

        permissionRepository.save(permission);
        return toPermissionResponseDTO(permission);
    }

    @Override
    public PermissionResponseDTO deletePermission(PermissionRequestDTO permissionRequestDTO) {
        return null;
    }

    @Override
    public List<PermissionResponseDTO> findPermissionsOfAFile(Long fileID) {
        List<Permission> permissionList = permissionRepository.findAllPermissionByFileID(fileID);
        return permissionList.stream().map(this::toPermissionResponseDTO).collect(Collectors.toList());
    }

    @Override
    public List<PermissionResponseDTO> findPermissionOfAnEmployee(Long employeeID) {
        List<Permission> permissionList = permissionRepository.findAllByEmployeeID(employeeID);
        return permissionList.stream().map(this::toPermissionResponseDTO).collect(Collectors.toList());
    }

    @Override
    public List<PermissionResponseDTO> findPermissionByFilter(PermissionFileFilterRequestDTO requestDTO) {
        List<Permission> permissionList = permissionRepository.findByFilter(requestDTO.getSearchValue(), requestDTO.getPermissionEnum(), requestDTO.getEmployeeID());
        return permissionList.stream().map(this::toPermissionResponseDTO).collect(Collectors.toList());
    }

    private boolean hasGrantPermission(Long grantingEmployeeID, Long fileID, PermissionEnum permissionGranting) {
        entityUtil.getEmployeeEntity(grantingEmployeeID);
        entityUtil.getFileEntity(fileID);

        Set<PermissionEnum> permissionCheckSet = new HashSet<>();
        if (PermissionEnum.OWNER.equals(permissionGranting)
                || PermissionEnum.SHARE.equals(permissionGranting)
                || PermissionEnum.DISABLED.equals(permissionGranting)) {
            //  Granting Owner or Share permission or Disable permission, require OWNER permission
            permissionCheckSet.add(PermissionEnum.OWNER);
        } else if (PermissionEnum.READ.equals(permissionGranting)) {
            //  Sharing, require OWNER or Sharing permission
            permissionCheckSet.add(PermissionEnum.OWNER);
            permissionCheckSet.add(PermissionEnum.SHARE);
        }

        return permissionRepository.hasPermissionInSet(grantingEmployeeID, fileID, permissionCheckSet);
    }

    private Permission toPermissionEntity(Permission permission, Employee granted, EmployeeFile file, PermissionEnum permissionEnum) {
        permission = permission == null ? new Permission() : permission;
        permission.setEmployee(granted);
        permission.setEmployeeFile(file);
        permission.setPermissionEnum(permissionEnum);
        return permission;
    }

    private PermissionResponseDTO toPermissionResponseDTO(Permission permission) {
        return PermissionResponseDTO.builder()
                .permissionID(permission.getPermissionID())
                .employeeID(permission.getEmployee().getEmployeeID())
                .employeeName(permission.getEmployee().getUsername())
                .fileID(permission.getEmployeeFile().getFileID())
                .fileName(permission.getEmployeeFile().getFileName())
                .permission(permission.getPermissionEnum())
                .createdOn(permission.getCreatedOn())
                .build();
    }
}
