package com.finall.service;

import com.finall.model.request.EmployeeCreateRequestDTO;
import com.finall.model.response.EmployeeResponseDTO;

import java.util.List;

public interface EmployeeService {

    EmployeeResponseDTO createEmployee(EmployeeCreateRequestDTO createRequestDTO);

    String deleteEmployee(Long employeeID);

    EmployeeResponseDTO validateLogin(EmployeeCreateRequestDTO requestDTO); // TEST

    void clearSession();

    List<EmployeeResponseDTO> findAllExceptCurrentOwner(Long currentOwnerID);
}
