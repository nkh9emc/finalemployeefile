package com.finall.service;

import com.finall.model.request.PermissionFileFilterRequestDTO;
import com.finall.model.request.PermissionRequestDTO;
import com.finall.model.response.PermissionResponseDTO;

import java.util.List;

public interface PermissionService {

    PermissionResponseDTO grantPermission(PermissionRequestDTO permissionRequestDTO);

    PermissionResponseDTO deletePermission(PermissionRequestDTO permissionRequestDTO);

    List<PermissionResponseDTO> findPermissionsOfAFile(Long fileID);

    List<PermissionResponseDTO> findPermissionOfAnEmployee(Long employeeID);

    List<PermissionResponseDTO> findPermissionByFilter(PermissionFileFilterRequestDTO requestDTO);
}
